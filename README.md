DazMod DIY modular system
=========================

The repository contains modular system projects ready for DIY building. Every directory in the repository is a separate module, case or accessories. Every project should contain svg panel ready for laser cutting, scheme of the electric circuit and a description.
