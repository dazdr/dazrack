Geyser by DazMod
================

The Geyser is Voltage Controlled Oscillator based on VCO-1 by Thomas Henry.

![Geyser design](https://gitlab.com/efedin/dazrack/raw/master/Geyser/Geyser.png)

Features:
* Sine, Pulse, Triangle waves
* Exponential FM
* 1v/Oct
* Coarse and Fine tuning
* Adjustable PWM

The difference between Thomas and my scheme:
* The resistor values are adapted for 12v eurorack PSI
* The lack of linear FM input.

Thank you Thomas Henry for the incredible circuit design!

**All rights to this design rest with Thomas Henry. I do not claim or grant any copyright or license to this design. I am not selling any boards.**